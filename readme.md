# Git - Config, Init e Clone

_git init_  
Faz com que o projeto use o Git

_git clone [url]_  
Baixa um projeto existente

_git remote -v_  
Lista todos os repositórios remotos

_git remote add origin [url]_  
Adiciona um repositório remoto ao projeto

_git config --list_  
Configurações do Git  
Existem três níveis de Configurações no Git:

- **Configuração do Sistema** vale para qualquer projeto e usuário da máquina
- **Configuração do Usuário** vale apenas para qualquer projeto que esteja no usuário apenas
- **Configuração do Projeto** é somente para o projeto

_git config --system --edit_  
Abrir para editar o arquivo de Configuração do Sistema

_git config --global --edit_  
Abrir para editar o arquivo de Configuração do Usuário

_git config --global core.editor code_  
Abrir para editar o arquivo de Configuração do usuário no VS Code

_git config --edit_  
Abrir para editar o arquivo de C onfiguração do Projeto

# Git - Alias, Status, Add, Commit, Amend e Stash

Status dos arquivos

- **Untracked** é o estado onde o arquivo ainda não foi rastreado;
- **Unmodified** - é o estado onde o arquivo não sofreu alterações em relação a sua referência;
- **Modified** - é o estado onde o arquivo sofreu alterações em relação a sua referência;
- **Staged** - é o estado onde o arquivo já foi endereçado e aguarda ser transferido ao repositório;

Ammend

_git commit --amend --no-edit_  
Faz o commit dos arquivos junto com o commit anterior

Stash

_git stash_  
Volta para como estava no último commit

_git stash apply_  
Volta as alterações mantendo o stash

_git stash pop_  
Volta as alterações limpando a stash

_git tag 1.0_  
Adiciona uma tag

_git tag 1.0 -m "release 1.0"_  
Adiciona uma tag anotada no último commit

_git tag -a "0.1.beta" -m "release 0.1.beta" 5b45219_  
Adiciona uma tag anotada em um commit específico

_git push origin master --tags_  
Envia todas as tags para o servidor

_git push origin master --follow-tags_
Só envia para o servidor as tags anotadas
